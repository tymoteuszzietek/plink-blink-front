export interface ITarget {
    id?: number;
    ip: string;
    program_id: number;
    actual_program_id: number;
    battery: string;
    last_time?: string;
    running: boolean;
}
