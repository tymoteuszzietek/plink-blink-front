export interface ISetting {
    id: number;
    key: string;
    value: number;
}
