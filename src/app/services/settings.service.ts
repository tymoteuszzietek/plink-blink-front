/* tslint:disable:no-unused-variable */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import {ISetting} from '../interfaces/isetting';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';

@Injectable()
export class SettingsService {
    private _settingEndPiont = 'http://pb2.local/api/setting';

    constructor(private _http: Http) {
    }

    list(): Observable<ISetting[]> {
        return this._http.get(this._settingEndPiont)
            .map(response => <ISetting[]>response.json());
    }

    update(setting: ISetting): Observable<boolean> {
        let body = JSON.stringify(setting);
        return this._http.put(this._settingEndPiont + setting.key, body).map(results => {
            return (results.status === 204);
        });
    }
}
