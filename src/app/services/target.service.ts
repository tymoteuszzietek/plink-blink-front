/* tslint:disable:no-unused-variable */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { ITarget } from '../interfaces/itarget';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';

@Injectable()
export class TargetService {
  private _targetListEndPiont = 'http://pb2.local/api/target';
  private _targetBlinEndPiont = 'http://pb2.local/api/targets-command/blink/';
  constructor(private _http: Http) {
  }

  list(): Observable<ITarget[]> {
    return this._http.get(this._targetListEndPiont)
        .map(response => <ITarget[]> response.json());
  }

  blink(target: ITarget): Observable<boolean> {
   return this._http.put(this._targetBlinEndPiont + target.ip, []).map(results => {
     return (results.status === 204);
   });
  }
}
