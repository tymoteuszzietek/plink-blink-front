import {Component, OnInit} from '@angular/core';
import {ITarget} from '../interfaces/itarget';
import {TargetService} from '../services/target.service';

@Component({
    selector: 'app-targets',
    templateUrl: './targets.component.html',
    styleUrls: ['./targets.component.css']
})
export class TargetsComponent implements OnInit {

    targets: ITarget[] = [];
    loading = true;
    interval: any;

    constructor(private _targetService: TargetService) {
    }

    ngOnInit() {
        this.loading = true;

        this.refreshData();
        this.interval = setInterval(() => {
            this.refreshData();
        }, 5000);
    }

    refreshData() {
        this._targetService.list()
            .subscribe(targets => {
                this.loading = false;
                this.targets = targets['data'];
            });
    }

    blink(target: ITarget) {
        this._targetService.blink(target)
            .subscribe(() => {});
    }

}
