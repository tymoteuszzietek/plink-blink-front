import { Component, OnInit } from '@angular/core';
import {ISetting} from '../interfaces/isetting';
import {SettingsService} from '../services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  settings: ISetting[] = [];
  loading = true;

  constructor(private _settingsService: SettingsService) {
  }

  ngOnInit() {
    this.loading = true;

    this._settingsService.list()
        .subscribe(settings => {
          this.loading = false;
          this.settings = settings['data'];
        });
  }
}
